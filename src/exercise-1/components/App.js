import React, {Component} from 'react';
import Home from "./Home";
import About from "./About";
import Profile from "./Profile";
import {BrowserRouter, Link} from "react-router-dom";
import {Route} from "react-router";
import Product from "./Product";
import Goods from "./Goods";

class App extends Component {
    render() {
        return (
            <div className="app">
                <BrowserRouter>
                    <Link to="/">Home</Link>
                    <Link to="/my-profile">My Profile</Link>
                    <Link to="/products">Products</Link>
                    <Link to="/about-us">About Us</Link>
                    <Link to="/goods">Goods</Link>
                    <Route path="/" component={Home}/>
                    <Route path="/products" component={Product}/>
                    <Route path="/my-profile" component={Profile}/>
                    <Route path="/about-us" component={About}/>
                    <Route path="/goods" component={Goods}/>
                    <Route component={Home}></Route>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;
