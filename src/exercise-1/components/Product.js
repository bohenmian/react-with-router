import React from "react";
import data from '../../exercise-2/mockups/data'
import {BrowserRouter, Link} from "react-router-dom";
import {Route} from "react-router";

const Product = () => {
    return (
        <div>
            <BrowserRouter>
                <Route path='/products' exact={true} component={Data}/>
                <Route path='/products/:id' component={ProductDetail}/>
            </BrowserRouter>
        </div>
    );
};

const ProductDetail = (match) => {
    const product = Object.values(data)[match.match.params.id - 1];
    return (
        <div>
            <p>Product Detail</p>
            <p>Name: {product.name}</p>
            <p>Price: {product["price"]}</p>
            <p>Quantity: {product["quantity"]}</p>
            <p>Desc: {product.desc}</p>
            <p>URL: {`/products/${product.id}`}</p>
        </div>
    );
};

const Data = () => {
    return (
        <div>
            {<p>All Products:</p>}
            {Object.keys(data).map((item, index) => {
                return (
                    <li>
                        <Link key={index} to={`/products/${Object.values(data)[index].id}`}>{item}</Link>
                    </li>
                )
            })}
        </div>
    );
};
export default Product;
